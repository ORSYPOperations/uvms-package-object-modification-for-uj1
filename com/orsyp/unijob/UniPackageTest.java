package com.orsyp.unijob;

import com.orsyp.UniverseException;
import com.orsyp.api.Context;
import com.orsyp.api.deploymentpackage.*;
import com.orsyp.api.deploymentpackage.Package;
import com.orsyp.api.job.Job;
import com.orsyp.help.OnlineHelp;
import com.orsyp.std.deploymentpackage.PackageListStdImpl;
import com.orsyp.std.deploymentpackage.PackageStdImpl;


public class UniPackageTest  {
	int iter;
	int iter_delay;
	Context context;

	public UniPackageTest(Context context) {
		this.context = context;
		this.iter = 1000000;
		this.iter_delay = 5000;
	}

	public void ListContent(String pckName) throws UniverseException {

		
		PackageList pkgList = new PackageList(this.context);
		pkgList.setImpl(new PackageListStdImpl()); 

		//test extract the list of packages
		try {
			pkgList.extract();
		} catch (UniverseException e1) {
			e1.printStackTrace();
			return;
		}
		boolean pckExist=false;
		for (int a = 0; a < pkgList.getCount(); a++) {
			PackageItem itemTemp = pkgList.get(a);
			if (itemTemp.getName().equals(pckName)){pckExist=true;}
		}
		
		if (pckName.equals("XXX_ALL_PACKAGES")){pckExist=true;}
		if (!pckExist){
			OnlineHelp.displayHelp(-1, "The Package "+pckName+" Does Not exist", "1.0");
		}

		for (int i = 0; i < pkgList.getCount(); i++) {
			PackageItem item = pkgList.get(i);
			PackageId ID = new PackageId(item.getName());
			if (item.getName().equals(pckName) || pckName.equals("XXX_ALL_PACKAGES")){
			//System.out.println("+++ Package Name [" +i+"] "+ ID.getPackageName());
			Package pck = new Package(context,ID);
			pck.setImpl(new PackageStdImpl());

			try {
				pck.extract();
			} catch (UniverseException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
				return;
			}
			System.out.println("+++ Retrieved Package:" + pck.getName());

			PackageElement[] pck_el = pck.getElements();

			System.out.println("    --> Number of elements in Pck " +item.getName()+ " : " + pck_el.length);
			int numJob = 0;
			for (int a = 0; a < pck_el.length; a++) {if (pck_el[a].getType().toString().equals("JOB_TEMPLATE")){numJob++;}}
			System.out.println("        --> Number of Jobs in Pck " +item.getName()+ " : " + numJob);
			
			for (int j = 0; j < pck_el.length; j++) {
				String typeis = pck_el[j].getType().toString();
				ApiConflictPolicy policy = null;
				pck_el[j].setConflictPolicy(policy.OVERWRITE);
				pck.addOrUpdatePackageElement(pck_el[j]);
				updatePackage(pck);
				
				if ( typeis.equals("JOB_TEMPLATE")){
					try {
						// Retrieve the job templates inside the package 
						PackageElement packJobTemplate = pck_el[j];

						// Display information
						//System.out.println("TEMPLATE PACKAGE"+j+" name:" + packJobTemplate.getName());
						//System.out.println("TEMPLATE PACKAGE "+j+" command:" + packJobTemplate.getJob().getCommand());
						//System.out.println("TEMPLATE PACKAGE "+j+" target:" + packJobTemplate.getJob().getTargetId());
						//System.out.println("--> Processing Job: " + packJobTemplate.getName());
						Job job = packJobTemplate.getJob();
						//job.setName("JYR");
						job.setTargetName("XXX_TARGET_DEFAULT");
						//pck.exportPackage(output)
						packJobTemplate.setJob(job);
						pck.addOrUpdatePackageElement(packJobTemplate);
						updatePackage(pck);
						//System.out.println("    +++ New Job Target Name: " + job.getTargetName());
					} catch (UniverseException e1) {
						// TODO Auto-generated catch block
						e1.printStackTrace();
						return;
					}
				}
				}
				System.out.println("+++ package "+item.getName()+" Processed, Jobs processed ["+numJob+"]\n");
			}else{
				System.out.println("%%% package "+item.getName()+" Skipped");
			}
		} 
	
		
	}

	public void updatePackage(Package pck) throws UniverseException {
		try{
			
			pck.setContext(this.context);
			PackageStdImpl pckStdImpl = new PackageStdImpl();
			pckStdImpl.init(pck);
			pck.setImpl(pckStdImpl);
			pck.save();
		} 
		catch (com.orsyp.api.ObjectAlreadyExistException oaee) {
			//TODO Do not output anything now.
			//oaee.printStackTrace();
		}		
		catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public String getName() {
		return this.getClass().getName();
	}
}

